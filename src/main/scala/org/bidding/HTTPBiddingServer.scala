package org.bidding

import akka.actor.{ActorRef, ActorSystem}
import akka.http.scaladsl.Http
import akka.stream.ActorMaterializer
import akka.http.scaladsl.server.Route
import scala.concurrent.Await
import scala.concurrent.duration.Duration


// Akka http server for serving http requests
object HTTPBiddingServer extends App with AuctionRoutes {

  // set up ActorSystem and other dependencies here

  implicit val system: ActorSystem = ActorSystem("Auction")
  implicit val materializer: ActorMaterializer = ActorMaterializer()
  //#server-bootstrapping


  val biddingAgentActor : ActorRef = system.actorOf(BiddingAgent.props("biddingAgent"), "biddingAgent")

  lazy val routes: Route = auctionRoutes

  //#http-server initializing
  Http().bindAndHandle(routes, "localhost", 8080)

  println(s"Bidding Server online at http://localhost:8080/")

  Await.result(system.whenTerminated, Duration.Inf)

}
