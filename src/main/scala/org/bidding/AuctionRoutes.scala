package org.bidding

import akka.actor.{ActorRef, ActorSystem}
import akka.event.Logging
import akka.http.scaladsl.model.{ContentTypes, HttpEntity, StatusCodes}
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.server.directives.MethodDirectives.post
import akka.pattern.ask
import akka.util.Timeout

import scala.concurrent.duration._

// Routing the requests from  client
trait AuctionRoutes extends JsonSupport with Domain {

  // we leave these abstract, since they will be provided by the App
  implicit def system: ActorSystem

  lazy val log = Logging(system, classOf[AuctionRoutes])

  // other dependencies that UserRoutes use
  def biddingAgentActor: ActorRef

  // Required by the `ask` (?) method below
  implicit lazy val timeout = Timeout(5.seconds) // usually we'd obtain the timeout from the system's configuration

  //#all-routes

  lazy val auctionRoutes: Route =
    pathPrefix("bid") {
      concat(
        pathEnd {
          concat(
            post {
              entity(as[BidRequest]) { bidRequest =>
                onSuccess(biddingAgentActor ? Bid(bidRequest)) {
                  case Some(value) => complete(HttpEntity(ContentTypes.`application/json`, value.toString))
                  case _ => complete(StatusCodes.NoContent)
                }
              }
            }
          )
        }
      )
    }
}
