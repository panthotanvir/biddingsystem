package org.bidding

//#json-support
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import spray.json.DefaultJsonProtocol

trait JsonSupport extends SprayJsonSupport with Domain {

  import DefaultJsonProtocol._

  // these are from spray-json
  implicit val geoFormat = jsonFormat4(Geo)
  implicit val deviceFormat = jsonFormat2(Device)
  implicit val userFormat = jsonFormat2(User)
  implicit val siteFormat = jsonFormat2(Site)
  implicit val impressionFormat = jsonFormat8(Impression)
  implicit val bidRequestFormat = jsonFormat5(BidRequest)

}

