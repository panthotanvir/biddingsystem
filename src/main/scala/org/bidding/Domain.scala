package org.bidding

// Use to predefined campaign list and case class declaration
trait Domain {
  final case class TimeRange(timeStart: Long, timeEnd: Long)
  final case class Targeting(cities: List[String], targetedSiteIds: List[Int]) // targetedSiteIds
  final case class Banner(id: Int, src: String, width: Int, height: Int)

  final case class Campaign(id: Int, userId: Int, country: String, runningTimes: Set[TimeRange], targeting: Targeting, banners: List[Banner], bid: Double)

  final case class Impression(id: String, wmin: Option[Int], wmax: Option[Int], w: Option[Int], hmin: Option[Int], hmax: Option[Int], h: Option[Int], bidFloor: Option[Double])
  final case class Site(id: Int, domain: String)
  final case class User(id: String, geo: Option[Geo])
  final case class Device(id: String, geo: Option[Geo])
  final case class Geo(country: Option[String], city: Option[String], lat: Option[Double], lon: Option[Double])

  final case class BidRequest(id: String, imp: Option[List[Impression]], site: Site, user: Option[User], device: Option[Device])

  final case class Bid(bidRequest: BidRequest)

  final case class BidResponse(id: String, bidRequestId: String, price: Double, adid: Option[String], banner: Option[Banner])

  val timeRange1: TimeRange = TimeRange(100, 200)
  val timeRange2: TimeRange = TimeRange(300, 400)
  val timeRange3: TimeRange = TimeRange(500, 600)

  val targeting1: Targeting = Targeting(List("Dhaka", "Delhi"), List(1, 2, 3))
  val targeting2: Targeting = Targeting(List("CTG", "RAJ"), List(4, 5, 6))

  val banner1: Banner = Banner(1, "Banner1", 1, 2)
  val banner2: Banner = Banner(2, "Banner2", 3, 4)
  val banner3: Banner = Banner(3, "Banner3", 5, 6)
  val banner4: Banner = Banner(4, "Banner4", 7, 8)

  val campaign1: Campaign = Campaign(1, 1, "Bangladesh", Set(timeRange1, timeRange2), targeting1, List(banner1, banner2), 1000)
  val campaign2: Campaign = Campaign(2, 2, "India", Set(timeRange2, timeRange3), targeting2, List(banner2, banner3), 2000)
  val campaign3: Campaign = Campaign(3, 3, "Nigeria", Set(timeRange3, timeRange1), targeting1, List(banner3, banner4), 3000)
  val campaign4: Campaign = Campaign(4, 4, "Lithuania", Set(timeRange1, timeRange2), targeting2, List(banner1, banner3), 4000)

  val campaignList : List[Campaign] = List(campaign1, campaign2, campaign3, campaign4)
}
